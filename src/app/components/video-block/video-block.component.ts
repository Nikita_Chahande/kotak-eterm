import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VideoPlayerComponent } from '../video-player/video-player.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video-block',
  templateUrl: './video-block.component.html',
  styleUrls: ['./video-block.component.css']
})
export class VideoBlockComponent implements OnInit {

  public user = {
    name: 'Izzat Nadiri',
    age: 26
  };

  constructor(public modalService: NgbModal) {}

  ngOnInit() {}

  openModal() {
    const modalRef = this.modalService.open(VideoPlayerComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.user = this.user;
    modalRef.result.then(result => {
      if (result) {
        console.log(result);
      }
    });
  }

}
