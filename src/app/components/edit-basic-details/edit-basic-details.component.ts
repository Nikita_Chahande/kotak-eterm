import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import Inputmask from 'inputmask';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { DateValidator } from '../../date-validator.validator';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-basic-details',
  templateUrl: './edit-basic-details.component.html',
  styleUrls: ['./edit-basic-details.component.css']
})
export class EditBasicDetailsComponent implements OnInit {
  @ViewChild('myInput')
  myInputElementRef!: ElementRef;
  @Input() userdetails = {}; 
  editDetailsForm: FormGroup;
  gender = 'male';
  tobacco = 'no';
  minDate = { year: 1920, month: 1, day: 1 };
  modalRef: any;
  
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder) { 
    this.editDetailsForm = this.fb.group({
      name: ['', Validators.required],
      birthdate: ['', Validators.required],
      email: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    console.log(this.userdetails);
    this.editDetailsForm.patchValue(this.userdetails)
  }

  setGender(val: string) {
    this.gender = val;
  }

  settobacco(val: string) {
    this.tobacco = val;
  }
  ngAfterViewInit(): void {
    Inputmask('datetime', {
    inputFormat: 'dd/mm/yyyy',
    placeholder: 'dd/mm/yyyy',
    alias: 'datetime',
    min: '01/01/2010',
    clearMaskOnLostFocus: false,
  }).mask(this.myInputElementRef.nativeElement);
  }

  close() {
    this.modalRef.close();
  }

}
