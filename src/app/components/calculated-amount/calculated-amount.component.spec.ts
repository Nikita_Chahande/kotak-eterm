import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatedAmountComponent } from './calculated-amount.component';

describe('CalculatedAmountComponent', () => {
  let component: CalculatedAmountComponent;
  let fixture: ComponentFixture<CalculatedAmountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalculatedAmountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatedAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
