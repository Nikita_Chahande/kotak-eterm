import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calculated-amount',
  templateUrl: './calculated-amount.component.html',
  styleUrls: ['./calculated-amount.component.css']
})
export class CalculatedAmountComponent implements OnInit {

  @Output("proceedWithStatus") proceedWithStatus: EventEmitter<any> = new EventEmitter();
  isDiscount:boolean= false;
  @Input() buttons: any = {};
  constructor(public router: Router) { }

  ngOnInit() {
  }

  proceedVal(){
    this.proceedWithStatus.emit();
  }

  getDiscount(){
    this.isDiscount !=  this.isDiscount;
  }
  root(){
    console.log(this.router.url);
    if (this.router.url === '/more-details'){
    //  return "make_payment";

  }
}

}
