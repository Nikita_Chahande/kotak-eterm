import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sorry-modal',
  templateUrl: './sorry-modal.component.html',
  styleUrls: ['./sorry-modal.component.css']
})
export class SorryModalComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal, public router: Router) { }

  ngOnInit(): void {
  }

  goHome() {
    this.router.navigate(['landing']);
  }

}
