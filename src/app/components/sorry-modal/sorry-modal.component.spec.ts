import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SorryModalComponent } from './sorry-modal.component';

describe('SorryModalComponent', () => {
  let component: SorryModalComponent;
  let fixture: ComponentFixture<SorryModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SorryModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SorryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
