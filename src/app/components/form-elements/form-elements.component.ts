import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DateValidator } from '../../date-validator.validator'

@Component({
  selector: 'app-form-elements',
  templateUrl: './form-elements.component.html',
  styleUrls: ['./form-elements.component.css']
})
export class FormElementsComponent implements OnInit {
  gender = 'male';
  personalAccidentCover: any;
  selectedHospitalAmount = '₹ 3,000';
  choosePlanForm!: FormGroup;
  residentialForm: FormGroup;
  minDate = { year: 1920, month: 1, day: 1 };
  formSubmitted = false;

  occuStatuses = [
    { label: 'Professional', value: 'Professional' },
    { label: 'Self Employed', value: 'Self Employed' },
    { label: 'Student', value: 'Student' },
    { label: 'Housewife', value: 'Housewife' },
    { label: 'Retired', value: 'Retired' },
    { label: 'Salaried', value: 'Salaried' },
  ];

  constructor(private fb: FormBuilder) {
    this.residentialForm = this.fb.group({
      occupation: ['', Validators.required],
      birthdate: ['', Validators.compose([Validators.required, DateValidator.dateValidator])],
    });
   }

  ngOnInit(): void {
  }

  setGender(val: string) {
    this.gender = val;
  }

  toggleBenefits(val: string) {
    if (val == 'personalAccidentCover') {
      this.personalAccidentCover = !this.personalAccidentCover;
    } 
  }


  decrease(field: string | number) {
    let policyAmount = this.choosePlanForm.controls[field].value;
    if (policyAmount > 0) {
      if (policyAmount === 10 || !policyAmount) {
        policyAmount -= 10;
      }
      else {
        policyAmount -= 1;
      }
    }
  }
  increase(field: string | number) {
    let policyAmount = this.choosePlanForm.controls[field].value;
    if (policyAmount === 0 || !policyAmount) {
      policyAmount = policyAmount + 10;
    }
    else {
      policyAmount++;
    }
  }

  assignDropdownVal(field: string | number, val: any) {
    this.residentialForm.controls[field].setValue(val);
  }


}
