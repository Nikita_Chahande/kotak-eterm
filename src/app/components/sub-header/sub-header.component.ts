import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditBasicDetailsComponent } from '../edit-basic-details/edit-basic-details.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent implements OnInit {
  @Input() partnerUser: any = {};
  constructor(public modalService: NgbModal) { }

  userdetails= {
    name: 'Shyam Verma',
    birthdate: '13071998',
    gender: 'Male',
    email: 'shyam@yahoo.com',
    mobile: '9090908282'
  }

  ngOnInit(): void {
    console.log(this.partnerUser)
  }
  editFormFeild() {
    const modalRef = this.modalService.open(EditBasicDetailsComponent,  {centered: true });
    modalRef.componentInstance.userdetails= this.userdetails;
    data: this.partnerUser
    modalRef.result.then(result => {
     
      if (result) {
        console.log(result);
      }
    });
  }

}
