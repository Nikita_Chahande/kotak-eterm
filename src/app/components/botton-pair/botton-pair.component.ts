import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SorryModalComponent } from '../sorry-modal/sorry-modal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-botton-pair',
  templateUrl: './botton-pair.component.html',
  styleUrls: ['./botton-pair.component.css']
})
export class BottonPairComponent implements OnInit {
  @Input() buttons: any = {};
  constructor(public modalService: NgbModal) { }

  ngOnInit(): void {
  }

  openSorryModal() {
    const modalRef = this.modalService.open(SorryModalComponent, { size: 'sm', centered: true });
    modalRef.result.then(result => {
      if (result) {
        console.log(result);
      }
    });
  }
}
