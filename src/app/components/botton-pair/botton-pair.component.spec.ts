import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BottonPairComponent } from './botton-pair.component';

describe('BottonPairComponent', () => {
  let component: BottonPairComponent;
  let fixture: ComponentFixture<BottonPairComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BottonPairComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BottonPairComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
