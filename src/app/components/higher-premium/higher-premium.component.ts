import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-higher-premium',
  templateUrl: './higher-premium.component.html',
  styleUrls: ['./higher-premium.component.css']
})
export class HigherPremiumComponent implements OnInit {
  selectedFreq: any;
  @Output() childButtonEvent = new EventEmitter();

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  selectFreq(val:any) {
   this.selectedFreq = val;
   this.childButtonEvent.emit(this.selectedFreq)
   console.log(this.selectedFreq)
  }

}
