import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detail-block',
  templateUrl: './detail-block.component.html',
  styleUrls: ['./detail-block.component.css']
})
export class DetailBlockComponent implements OnInit {
  @Input() reasonsArray: any = {};
  
  constructor() { }

  ngOnInit(): void {
  }

}
