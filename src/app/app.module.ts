import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SubHeaderComponent } from './components/sub-header/sub-header.component';
import { SorryModalComponent } from './components/sorry-modal/sorry-modal.component';
import { VideoBlockComponent } from './components/video-block/video-block.component';
import { VideoPlayerComponent } from './components/video-player/video-player.component';
import { CalculatedAmountComponent } from './components/calculated-amount/calculated-amount.component';
import { EditBasicDetailsComponent } from './components/edit-basic-details/edit-basic-details.component';
import { FormElementsComponent } from './components/form-elements/form-elements.component';
import { EligibilityCriteriaComponent } from './pages/eligibility-criteria/eligibility-criteria.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { AddOnsComponent } from './pages/add-ons/add-ons.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { BottonPairComponent } from './components/botton-pair/botton-pair.component';
import { DetailBlockComponent } from './components/detail-block/detail-block.component';
import { TabsetComponent } from './components/tabset/tabset.component';
import { CustomizePlanComponent } from './pages/customize-plan/customize-plan.component';
import { MoreDetailsComponent } from './pages/more-details/more-details.component';
import { HigherPremiumComponent } from './components/higher-premium/higher-premium.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SubHeaderComponent,
    SorryModalComponent,
    VideoBlockComponent,
    VideoPlayerComponent,
    CalculatedAmountComponent,
    EditBasicDetailsComponent,
    FormElementsComponent,
    EligibilityCriteriaComponent,
    LandingPageComponent,
    AddOnsComponent,
    BottonPairComponent,
    DetailBlockComponent,
    TabsetComponent,
    CustomizePlanComponent,
    MoreDetailsComponent,
    HigherPremiumComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    // HomeRoutingModule,
    // ScrollingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule,
    NgSelectModule,
    BrowserAnimationsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
