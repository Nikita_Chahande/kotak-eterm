import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddOnsComponent } from '../pages/add-ons/add-ons.component';
import { EligibilityCriteriaComponent } from '../pages/eligibility-criteria/eligibility-criteria.component';
import { LandingPageComponent } from '../pages/landing-page/landing-page.component';

const routes: Routes = [

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RouteModuleRoutingModule { }
