import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import * as converter from 'number-to-words';
import { SorryModalComponent } from 'src/app/components/sorry-modal/sorry-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-eligibility-criteria',
  templateUrl: './eligibility-criteria.component.html',
  styleUrls: ['./eligibility-criteria.component.css'],
})
export class EligibilityCriteriaComponent implements OnInit {
  eligibilityCriteriaForm: FormGroup;
  formSubmitted = false;
  customerAge = 0;
  minDate = { year: 1920, month: 1, day: 1 };
  acceptPolicy = true;
  annual: any = '5,00,000';
  annualIncome:any;
  outputWords = '';


  reasonsArray = [
    {
      title: 'Title 1',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore',
      icon: 'assets/img/place-holder.png',
    },
    {
      title: 'Title 2',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore',
      icon: 'assets/img/place-holder.png',
    },
    {
      title: 'Title 3',
      desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore',
      icon: 'assets/img/place-holder.png',
    },
  ];
  occupationStatuses = [
    { label: 'Professional', value: 'Professional' },
    { label: 'Self Employed', value: 'Self Employed' },
    { label: 'Student', value: 'Student' },
    { label: 'Housewife', value: 'Housewife' },
    { label: 'Retired', value: 'Retired' },
    { label: 'Salaried', value: 'Salaried' },
  ];

  incomeStatuses = [
    { label: '5,00,000', value: '500000' },
    { label: '6,00,000', value: '600000' },
    { label: '7,00,000', value: '700000' },
    { label: '8,00,000', value: '800000' },
    { label: '9,00,000', value: '900000' },
  ];

  educationStatuses = [
    { label: 'Graduate', value: 'graduate' },
    { label: 'Undergraduate', value: 'undergraduate' },
    { label: 'Postgraduate', value: 'postgraduate' },
  ];

  nationalityStatuses = [
    { label: 'Resident indian', value: 'indian' },
    { label: 'Non-resident indian', value: 'nonIndian' },
  ];

  constructor(public router: Router, private fb: FormBuilder, public modalService: NgbModal) {
    this.eligibilityCriteriaForm = this.fb.group({
      occupation: ['', Validators.required],
      income: [this.annualIncome, Validators.required],
      education: ['', Validators.required],
      nationality: ['', Validators.required],
      pincode: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

  // navigate() {
  //   this.router.navigate(['customize-plan']);
  // }

  assignDropdownVal(field: string | number, val: any) {
    this.eligibilityCriteriaForm.controls[field].setValue(val);
  }

  navigate() {
    this.formSubmitted = true;
    const checkEligibility = this.annualIncome;
    
    if (this.eligibilityCriteriaForm.valid) { 
      if(checkEligibility<=30000) {
        const modalRef = this.modalService.open(SorryModalComponent, { size: 'sm', centered: true });
        modalRef.result.then(result => {
          if (result) {
            console.log(result);
          }
        });
      }
      else {
        this.router.navigate(['customize-plan']);
      }
    } else {
      setTimeout(() => {
        this.moveToError();
      }, 500);
    }
  }

  validate(evt: any) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  moveToError() {
    // var elt = $(".errorInput");
    // if (elt.length) {
    //   $('html, body').animate({
    //     scrollTop: (elt.first().offset().top) - 90
    //   }, 500);
    // }
  }

  isNumberKey(event: any) {
    var charCode = event.which ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  numberWithCommas() {
    if (this.eligibilityCriteriaForm.controls['income'].value != '') {
      var result = this.eligibilityCriteriaForm.controls[
        'income'
      ].value.replace(/,/g, '');
      result = result.toString();
      var lastThree = result.substring(result.length - 3);
      var otherNumbers = result.substring(0, result.length - 3);
      if (otherNumbers != '') lastThree = ',' + lastThree;
      var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree;
      this.eligibilityCriteriaForm.controls['income'].setValue(res);
      
    this.outputWords = converter.toWords(res.replace(/,/g, ''));
    console.log(this.outputWords);
    }
    else {
      this.outputWords ='';
    }

  }
}
