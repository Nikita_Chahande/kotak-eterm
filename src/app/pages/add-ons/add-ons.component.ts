import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-ons',
  templateUrl: './add-ons.component.html',
  styleUrls: ['./add-ons.component.css']
})
export class AddOnsComponent implements OnInit {
  lifeSecure: any;
  addOnsForm: FormGroup;
  accidentCover = 0;
  plusDisabled = false;
  personalShield = false;

  buttons={
    pair1: 'BACK',
    pair2: 'PROCEED'
  }

  constructor(public router: Router, private fb: FormBuilder) { 
    this.addOnsForm = this.fb.group({
      
    });
  }

  ngOnInit(): void {
  }

  toggleBenefits(val: string) {
    if (val == 'lifeSecure') {
      this.lifeSecure = !this.lifeSecure;
    } 
  }

  decrease(val: number) {
    this.plusDisabled = false;
    if (this.accidentCover > val) {
      if (this.accidentCover === 10 || !this.accidentCover) {
        this.accidentCover -= 10;
      } else {
        this.accidentCover -= 1;
      }
    } else {
      this.personalShield = false;
    }
  }
  increase(val: number) {
    this.personalShield = true;
    if (this.accidentCover < val) {
      if (this.accidentCover === 0 || !this.accidentCover) {
        this.accidentCover = this.accidentCover + 10;
      } else {
        this.accidentCover++;
      }
      if (this.accidentCover == val) {
        this.plusDisabled = true;
      } else {
        this.plusDisabled = false;
      }
    }
  }

  proceed() {
    this.router.navigate(['more-details']);
  }

}
