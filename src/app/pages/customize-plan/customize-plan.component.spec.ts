import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizePlanComponent } from './customize-plan.component';

describe('CustomizePlanComponent', () => {
  let component: CustomizePlanComponent;
  let fixture: ComponentFixture<CustomizePlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomizePlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
