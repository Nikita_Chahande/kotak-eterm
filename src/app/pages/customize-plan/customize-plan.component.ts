import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormGroup, FormBuilder, Validators, FormControl, FormsModule} from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HigherPremiumComponent } from 'src/app/components/higher-premium/higher-premium.component';
import * as converter from 'number-to-words';

@Component({
  selector: 'app-customize-plan',
  templateUrl: './customize-plan.component.html',
  styleUrls: ['./customize-plan.component.css'],
})
export class CustomizePlanComponent implements OnInit {
  customizePlanForm: FormGroup;
  formSubmitted = false;
  customerAge = 0;
  minDate = { year: 1920, month: 1, day: 1 };
  acceptPolicy = true;
  selectedFreq ="Annually";
  private modalRef: any;
  closeResult= '';

  annualsum = '50,00,000';

  outputWords = '';
  convertToWord() {
    this.outputWords = converter.toWords(this.annualsum);
    console.log(this.outputWords);
  }


  sumStatuses = [
    { label: '5,00,000', value: '500000' },
    { label: '6,00,000', value: '600000' },
    { label: '7,00,000', value: '700000' },
    { label: '8,00,000', value: '800000' },
    { label: '9,00,000', value: '900000' },
  ];

  policyStatuses = [
    { label: '40 years', value: '40' },
    { label: '50 years', value: '50' },
    { label: '60 years', value: '60' },
    { label: '70 years', value: '70' },
  ];

  payingStatuses = [
    { label: 'Single Premium', value: 'SinglePremium', offer: 'Save 2%' },
    { label: '5 years', value: '5years', offer: 'Save 10%' },
    { label: '10 years', value: '10years', offer: 'Save 6%' },
    { label: '40 years', value: '40years', offer: 'Same as policy term' },
  ];

  feqStatuses = [
    { label: 'Annually', value: 'annually' },
    { label: 'Monthly', value: 'monthly' },
  ];

  payoutStatuses = [
    {
      label: 'Immediate',
      value: 'immediate',
      desc: 'Receive the entire coverage amount in one single payout.',
    },
    {
      label: 'Level Recurring Payout',
      value: 'LevelRecurringPayout',
      desc: 'Receive a part of the coverage amount as a lump sum payout and the rest in monthly instalments.',
    },
    {
      label: 'Increasing Recurring Payout',
      value: 'IncreasingRecurringPayout',
      desc: 'Receive a part of the coverage amount as a lump sum payout and the rest in monthly instalments.',
    },
  ];

  buttons={
    pair1: 'BACK',
    pair2: 'PROCEED'
  }

  values: any;


  constructor(
    public router: Router,
    private fb: FormBuilder,
    public modalService: NgbModal,
  ) {
    this.customizePlanForm = this.fb.group({
      sum: ['50,00,000', Validators.required],
      policy: ['40 years', Validators.required],
      paying: ['40 years', Validators.required],
      frequency: ['Anually', Validators.required],
      payout: ['Immediate', Validators.required],
    });
  }

  ngOnInit(): void {}

  assignDropdownVal(field: string | number, val: any) {
    this.customizePlanForm.controls[field].setValue(val);
  }

  // statusNavigate() {
  //   this.router.navigate(['add-ons']);
  // }

  statusNavigate() {
    this.formSubmitted = true;
    if (this.customizePlanForm.valid) {
      this.router.navigate(['add-ons']);
    } else {
      setTimeout(() => {
        this.moveToError();
      }, 500);
    }
  }

  validate(evt: any) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  moveToError() {
    // var elt = $(".errorInput");
    // if (elt.length) {
    //   $('html, body').animate({
    //     scrollTop: (elt.first().offset().top) - 90
    //   }, 500);
    // }
  }

  checkFreq(field: string | number, val: any, content: any) {
    this.customizePlanForm.controls[field].setValue(val);
    if (val == 'Monthly') {
      this.modalRef = this.modalService.open(content, {centered: true});
      this.modalRef.result.then(
        (result: any) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason: any) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          console.log(this.closeResult);
        }
      );
    }
    
  }
  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }

  selectFreq(val:any) {
    this.selectedFreq = val;
    console.log(this.selectedFreq);
    this.modalRef.close();
   }

   close() {
    this.modalRef.close();
   }

   isNumberKey(event: any) {
    var charCode = event.which ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  numberWithCommas() {

    if (this.customizePlanForm.controls['sum'].value != '') {
      var result = this.customizePlanForm.controls[
        'sum'
      ].value.replace(/,/g, '');
      result = result.toString();
      var lastThree = result.substring(result.length - 3);
      var otherNumbers = result.substring(0, result.length - 3);
      if (otherNumbers != '') lastThree = ',' + lastThree;
      var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree;
      this.customizePlanForm.controls['sum'].setValue(res);
      
    this.outputWords = converter.toWords(res.replace(/,/g, ''));
    console.log(this.outputWords);
    }
    else {
      this.outputWords ='';
    }

  }

}
