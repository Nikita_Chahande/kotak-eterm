import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { DateValidator } from '../../date-validator.validator';
import { OwlOptions } from 'ngx-owl-carousel-o';
import Inputmask from 'inputmask';

/* For Typehead */
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

declare var $: any;

const emailList = [
  'rajesh.wadhwa@gmail.com',
  'rajesh.wadhwa@yahoo.com',
  'rajesh.wadhwa@outlook.com',
];

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
})
export class LandingPageComponent implements OnInit {
  @ViewChild('myInput')
  myInputElementRef!: ElementRef;

  formSubmitted = false;
  landingPageForm: FormGroup;
  gender = 'male';
  tobacco = 'no';
  customerAge = 0;
  showMinor = false;
  minDate = { year: 1920, month: 1, day: 1 };
  acceptPolicy = true;
  showTerm = true;

  customOptions: OwlOptions = {
    loop: true,
    margin: 0,
    nav: false,
    lazyLoad: true,
    autoplay: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    // autoplayTimeout: 3000,
    autoplaySpeed:800,
    autoplayTimeout:3000,
    autoplayHoverPause: false,
    dots: true,
    items: 1,
    navSpeed: 700,
  };

  /* For Typehead */
  public emaildModel: any;
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map((term) =>
        term.length < 2
          ? []
          : emailList
              .filter((v) => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
              .slice(0, 10)
      )
    );

  constructor(public router: Router, private fb: FormBuilder) {
    this.landingPageForm = this.fb.group({
      name: ['', [Validators.required]],
      birthdate: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required]],
      emailId: ['', [Validators.required, Validators.email]], 
    });
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    Inputmask('datetime', {
    inputFormat: 'dd/mm/yyyy',
    placeholder: 'dd/mm/yyyy',
    alias: 'datetime',
    min: '01/01/2010',
    clearMaskOnLostFocus: false,
  }).mask(this.myInputElementRef.nativeElement);
  }

  setGender(val: string) {
    this.gender = val;
  }

  navigate() {
    this.formSubmitted = true;
    if (this.landingPageForm.valid && this.acceptPolicy) {
      this.router.navigate(['eligibility-criteria']);
    } else {
      setTimeout(() => {
        this.moveToError();
      }, 500);
    }
  }

  gotoResume() {
    this.router.navigate(['resume-page']);
  }

  setTobacco(val: any) {
    this.tobacco = val;
  }

  validate(evt: any) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  moveToError() {
    var elt = $('.errorInput');
    if (elt.length) {
      $('html, body').animate(
        {
          scrollTop: elt.first().offset().top - 90,
        },
        500
      );
    }
  }

  termClicked(status: any) {
    if (status) {
      this.showTerm = false;
    } else {
      this.showTerm = true;
    }
  }
}
