import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormElementsComponent } from './components/form-elements/form-elements.component';
import { AddOnsComponent } from './pages/add-ons/add-ons.component';
import { CustomizePlanComponent } from './pages/customize-plan/customize-plan.component';
import { EligibilityCriteriaComponent } from './pages/eligibility-criteria/eligibility-criteria.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { MoreDetailsComponent } from './pages/more-details/more-details.component';

const routes: Routes = [
  { path: '', redirectTo: '/landing', pathMatch: 'full' },
  { path: 'landing', component: LandingPageComponent, },
  { path: 'add-ons', component: AddOnsComponent },
  { path: 'eligibility-criteria', component: EligibilityCriteriaComponent },
  { path: 'customize-plan', component: CustomizePlanComponent },
  { path: 'more-details', component: MoreDetailsComponent},
  { path: 'elements', component: FormElementsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
